if(!String.prototype.format){
	String.prototype.format = function() {
		var args = Array.prototype.slice.call(arguments);
		return this.replace(/\{(\d+)\}/g, 
			function(m, i){
				return args[i];
			});
	};
}